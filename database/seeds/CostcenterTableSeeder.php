<?php

use Illuminate\Database\Seeder;
use App\Models\Costcenter\Costcenter;

class CostcenterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $costcenters = [
            [
                'code' => 254,
                'company' => 'Fleetwise',
                'reportsname' => 'Fleetwise new setup',
                'address' => 'Fleetwise',
                'telephone' => '254710528746',
                'user_id' => 1
            ],
            [
                'code' => 999,
                'company' => 'Unkown',
                'reportsname' => 'Unkown data import',
                'address' => 'Unkown',
                'telephone' => '100000000000',
                'user_id' => 1
            ]
        ];

        foreach ($costcenters as $center){
            Costcenter::create($center);
        }
    }
}
