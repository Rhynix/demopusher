<?php

use Illuminate\Database\Seeder;
use App\Models\User\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = config('app.adminpass');

        $users = [
            [
                'firstname' => 'John',
                'lastname' => 'Wanyoike',
                'email' => 'jhnwanyoike@gmail.com',
                'password' => bcrypt($password),
                'costCenter' => 254,
                'roleid' => 3,
                'accountstatus' => 1,
                'avatar' => 'john.png',
                'phone' => '254710528746',
                'theme' => 2,
                'user_id' => 2,
            ],
            [
                'firstname' => 'Wilfred',
                'lastname' => 'Obonyo',
                'email' => 'wilfred@gmail.com',
                'password' => bcrypt($password),
                'costCenter' => 254,
                'roleid' => 2,
                'accountstatus' => 1,
                'avatar' => 'wilfred.jpg',
                'phone' => '254733893754‬',
                'theme' => 1,
                'user_id' => 1,
            ],
            [
                'firstname' => 'Rhynix',
                'lastname' => 'John',
                'email' => 'jhnwanyoike@icloud.com',
                'password' => bcrypt($password),
                'costCenter' => 254,
                'roleid' => 1,
                'accountstatus' => 2,
                'avatar' => 'rhynix.jpg',
                'phone' => '254710528746',
                'theme' => 1,
                'user_id' => 1,
            ]
        ];

        foreach ($users as $user){
            User::create($user);
        }
    }
}
