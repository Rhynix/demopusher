<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname', 15);
            $table->string('lastname', 15);
            $table->string('email', 100)->unique();
            $table->string('uuid', 200);
            $table->string('password', 200);
            $table->enum('roleid', [1, 2, 3]);
            $table->integer('accountstatus');
            $table->integer('screenlock')->nullable();
            $table->integer('online')->nullable();
            $table->string('avatar');
            $table->string('phone');
            $table->string('theme');
            $table->integer('costCenter')->unsigned()->index();
            $table->integer('user_id');
            $table->timestamps();

            $table->foreign('costCenter')->references('code')->on('costcenters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
