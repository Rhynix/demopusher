<?php

namespace App\Http\Resources\Leaveusers;

use Illuminate\Http\Resources\Json\JsonResource;

class LeaveusersResourse extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'lable' => ($this->roleid) ? $this->uuid : $this->id,
            'value' => trim($this->lastname.' '.$this->firstname.' '.$this->firstName.' '.$this->otherName),
            'avatar' => $this->avatar,
            'usertype' => ($this->roleid) ? 1 : 2,
        ];
    }
}
