<?php

namespace App\Http\Resources\Leavetypes;

use Illuminate\Http\Resources\Json\JsonResource;

class LeavetypeResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'lable' => $this->id,
            'value' => $this->name .' ('.$this->numberofdays.')',
            'days' => $this->numberofdays,
        ];

        // return parent::toArray($request);
    }
}
