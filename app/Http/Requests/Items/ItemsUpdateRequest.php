<?php

namespace App\Http\Requests\Items;

use Illuminate\Foundation\Http\FormRequest;

class ItemsUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'              => 'required|integer|exists:items,id',
            'partCode'        => 'required|alpha_dash|min:2|max:50',
            'partDescription' => 'required|min:2|max:50',
            'unitDescription' => 'required|max:9|min:2',
            'partCategory'    => 'required',
            'lastPrice'       => 'required|numeric',
            'reorderLevel'    => 'required|numeric',
            'stockType'       => 'required',
            'supplierOne'     => 'required'
        ];
    }
}
