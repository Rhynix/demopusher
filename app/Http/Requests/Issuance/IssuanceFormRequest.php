<?php

namespace App\Http\Requests\Issuance;

use Illuminate\Foundation\Http\FormRequest;

class IssuanceFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dateIssued'        => 'required | date',
            'fueledVehicle'     => 'required | integer',
            'location'          => 'required | numeric',
            'currentOdometer'   => 'required | numeric | min:'.request()->previousOdometer,
            'previousOdometer'  => 'required | numeric',
            'fuelType'          => 'required',
            'quantity'          => 'required | numeric',
            'unitCost'          => 'required | numeric',
            'totalCost'         => 'required | numeric',
            'internalOrExternal'=> 'required',
            'department'        => 'required'
        ];
    }
}
