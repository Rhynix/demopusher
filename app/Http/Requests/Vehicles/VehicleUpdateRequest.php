<?php

namespace App\Http\Requests\Vehicles;

use Illuminate\Foundation\Http\FormRequest;

class VehicleUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vehicle' => 'required|alpha_dash|min:3|max:10|exists:vehicles,vehicle',
            'make' => 'required',
            'fuelType' => 'required',
            'model' => 'required',
            'vehicleType' => 'required',
            'baseLocation' => 'required',
            'department' => 'required',
            'costCenter' => 'required',
            'vehicleStatus' => 'required'
        ];
    }
}
