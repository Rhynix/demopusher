<?php

namespace App\Http\Requests\Tankslevels;

use Illuminate\Foundation\Http\FormRequest;

class TankUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'PumpName'   => 'required|numeric|exists:tankslevels,code',
            'fuelType'   => 'required',
            'capacity'   => 'required|numeric',
        ];
    }
}
