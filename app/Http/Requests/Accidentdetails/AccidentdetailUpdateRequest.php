<?php

namespace App\Http\Requests\Accidentdetails;

use Illuminate\Foundation\Http\FormRequest;

class AccidentdetailUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'            => 'required',
            'accidentType'  => 'required',
            'accidentNumber'=> 'required|numeric',
            'name'          => 'required',
        ];
    }
}
