<?php

namespace App\Http\Requests\Claims;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;
use App\Rules\AmountOrKilometePrecent;

class ClaimUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'            => 'required',
            'vehicle'       => 'required',
            'department'    => 'required',
            'startDate'     => 'required|date',
            'endDate'       => 'required|date|after_or_equal:'.Carbon::parse(request()->startDate)->format('jS M Y'),
            'claimerName'   => 'required',
            'claimDate'     => 'required|date',
            'kilometers'    => new AmountOrKilometePrecent(request()->amount),
            'destination'   => 'required',
        ];
    }
}
