<?php

namespace App\Http\Requests\Maintenancetypes;

use Illuminate\Foundation\Http\FormRequest;

class MaintenancetypesUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required | numeric | exists:maintenancetypes,id',
            'description' => 'required|max:20|min:3',
        ];
    }
}
