<?php

namespace App\Http\Requests\Booking;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;
class BookingUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'            => 'required',
            'dateFrom'      => 'required | date',
            'timeFrom'      => 'required',
            'dateTo'        => 'required | date | after_or_equal:'.Carbon::parse(request()->dateFrom)->format('jS M Y'),
            'timeTo'        => 'required',
            'vehicle'       => 'required',
            'driver'        => 'required',
            'destination'   => 'required',
            'personIncharge'=> 'required',
            'driverMobile' => 'required|digits:12|phone_number',
            'inchergerMobile' => 'required|digits:12|phone_number',
        ];
    }

    public function messages()
    {
        return [
            'driverMobile.phone_number' => 'Must be 12-digit phone number (e.g 254711111111) and must not include spaces or special characters',
            'inchergerMobile.phone_number' => 'Must be 12-digit phone number (e.g 254711111111) and must not include spaces or special characters',
        ];
    }
}
