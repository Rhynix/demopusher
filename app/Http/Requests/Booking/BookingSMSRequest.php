<?php

namespace App\Http\Requests\Booking;

use App\Http\Requests\FormRequest;
use Carbon\Carbon;
class BookingSMSRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'bookinglNumber' => 'required',
            'dateFrom' => 'required | date',
            'dateTo'   => 'required | date | after_or_equal:'.Carbon::parse(request()->dateFrom)->format('jS M Y'),
            'vehicle' => 'required',
            'driverMobile' => 'required|phone_number',
            'inchergerMobile' => 'required|phone_number',
            'destination' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'driverMobile.phone_number' => 'Must be 12-digit phone number (e.g 254711111111) and must not include spaces or special characters',
            'inchergerMobile.phone_number' => 'Must be 12-digit phone number (e.g 254711111111) and must not include spaces or special characters',
        ];
    }
}
