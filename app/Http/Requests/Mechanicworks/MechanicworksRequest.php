<?php

namespace App\Http\Requests\Mechanicworks;

use Illuminate\Foundation\Http\FormRequest;

class MechanicworksRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'jobNumber'     => 'required|numeric|exists:jobcards,id',
            'vehicle'       => 'required',
            'staff'         => 'required|numeric',
            'hoursWorked'   => 'required|numeric',
            'dateWorked'    => 'required|date',
            'workDone'      => 'required',
        ];
    }
}
