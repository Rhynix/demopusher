<?php

namespace App\Http\Requests\Itemsreturns;

use Illuminate\Foundation\Http\FormRequest;

class ItemsreturnsFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'previousQuantity' => 'required | numeric',
            'newQuantity' => 'required | numeric | max:'.request()->previousQuantity,
            'reason' => 'required',
            'JobNumber' => 'required',
            'UnitPrice' => 'required | numeric',
            'JobTotalCost' => 'required | numeric',
            'vehicle' => 'required',
            'PartNumber' => 'required',
            'partDescription' => 'required'
        ];
    }
}
