<?php

namespace App\Http\Requests\Messages;

use App\Http\Requests\FormRequest;

class MessagesFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'to' => 'required|integer',
            'message' => 'required|min:2|max:300',
        ];
    }
}
