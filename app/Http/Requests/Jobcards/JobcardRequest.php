<?php

namespace App\Http\Requests\Jobcards;

use Illuminate\Foundation\Http\FormRequest;

class JobcardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vehicle'           => 'required',
            'vehicleModel'      => 'required',
            'startDate'         => 'required | date',
            'startTime'         => 'required',
            'serviceType'       => 'required',
            'jobcardStatus'     => 'required',
            'department'        => 'required',
            'serviceDescription'=> 'required'
        ];
    }
}
