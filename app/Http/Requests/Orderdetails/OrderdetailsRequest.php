<?php

namespace App\Http\Requests\Orderdetails;

use Illuminate\Foundation\Http\FormRequest;

class OrderdetailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dateRecieved'      => 'required|date',
            'oderId'            => 'required|numeric',
            'partDescription'   => 'required|numeric',
            'quantity'          => 'required|numeric',
            'unitCost'          => 'required|numeric',
            'totalCost'         => 'required|numeric',
            'unit'              => 'required|max:5',
        ];
    }
}
