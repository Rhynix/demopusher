<?php

namespace App\Http\Requests\Vehiclebadge;

use Illuminate\Foundation\Http\FormRequest;

class VehiclebadgesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vehicle' => 'required|numeric',
            'badge' => 'required|numeric',
            'issueDate' => 'required|date',
            'expiryDate' => 'required|date',
            'cost' => 'required|numeric',
            'status' => 'required|numeric',
        ];
    }
}
