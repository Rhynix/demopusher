<?php

namespace App\Http\Requests\Accidents;

use Illuminate\Foundation\Http\FormRequest;

class AccidentUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'            => 'required',
            'vehicle'       => 'required',
            'accidentDetails'=> 'required',
            'driver'=> 'required|numeric',
            'accidentLocation'=> 'required',
            'accTime'=> 'required',
            'accidentDate'  => 'required | date'
        ];
    }
}
