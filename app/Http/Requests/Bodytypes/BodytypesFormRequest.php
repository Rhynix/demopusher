<?php

namespace App\Http\Requests\Bodytypes;

use App\Http\Requests\FormRequest;

class BodytypesFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required|min:3|max:15|unique:bodytypes,description'
        ];
    }
}
