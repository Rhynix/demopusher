<?php

namespace App\Http\Requests\Tankdetails;

use Illuminate\Foundation\Http\FormRequest;

class TankdetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fuelType'          => 'required',
            'lpoNumber'         => 'required|unique:tankheaders,lpoNumber',
            'pumpName'          => 'required',
            'capacity'          => 'required|numeric',
            'supplier'          => 'required|numeric',
            'newRecievedLitres' => 'required|numeric',
            'NewDateRecieved'   => 'required|date',
            'newCostPerLitre'   => 'required|numeric',
            'newBalance'        => 'required|numeric|max:'.request()->capacity
        ];
    }
}
