<?php

namespace App\Http\Requests\Costcenter;

use Illuminate\Foundation\Http\FormRequest;

class CotcenterFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'companyCode' => 'required | numeric | digits:3 | unique:costcenters,code',
            'company' => 'required',
            'companyAddress' => 'required',
            'reportsName' => 'required|max:25',
            'companyTelephone' => 'required'
        ];
    }
}
