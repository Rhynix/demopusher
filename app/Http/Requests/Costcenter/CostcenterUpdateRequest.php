<?php

namespace App\Http\Requests\Costcenter;

use App\Http\Requests\FormRequest;

class CostcenterUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'companyCode'   => 'required|numeric|exists:costcenters,code',
            'company'       => 'required',
            'reportsName'   => 'required|max:25',
            'companyAddress' => 'required',
            'companyTelephone' => 'required'
        ];
    }
}
