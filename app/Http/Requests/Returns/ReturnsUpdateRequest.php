<?php

namespace App\Http\Requests\Returns;

use Illuminate\Foundation\Http\FormRequest;

class ReturnsUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'            => 'required|exists:returns',
            'vehicle'       => 'required',
            'dateFrom'      => 'required | date',
            'kilometersDone'=> 'required_without:amount',
            'amount'        => 'required_without:kilometersDone',
            'department'    => 'required',
            'ratePerKm'     => 'required | numeric'
        ];
    }
}
