<?php

namespace App\Http\Requests\Returns;

use Illuminate\Foundation\Http\FormRequest;

class ReturnsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vehicle'       => 'required|numeric',
            'dateFrom'      => 'required|date',
            'kilometersDone'=> 'required_without:Amount',
            'Amount'        => 'required_without:kilometersDone',
            'department'    => 'required',
            'ratePerKm'     => 'required | numeric'
        ];
    }
}
