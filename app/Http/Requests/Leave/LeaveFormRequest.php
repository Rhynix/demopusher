<?php

namespace App\Http\Requests\Leave;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;

class LeaveFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dateFrom' => 'required|date',
            'dateTo'   => 'required|date|after_or_equal:'.Carbon::parse(request()->dateFrom)->format('jS M Y'),
            'driver' => 'required',
            'leaveState' => 'required|numeric',
            'leaveType' => 'required|numeric'
        ];
    }
}
