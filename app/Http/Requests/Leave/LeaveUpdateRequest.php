<?php

namespace App\Http\Requests\Leave;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;

class LeaveUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'            => 'required|numeric',
            'leaveState'    => 'required|numeric',
            'dateFrom'      => 'required|date',
            'dateTo'        => 'required|date|after_or_equal:'.Carbon::parse(request()->dateFrom)->format('jS M Y'),
            'driver'        => 'required',
        ];
    }
}
