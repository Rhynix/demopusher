<?php

namespace App\Http\Requests\Drivers;

use Illuminate\Foundation\Http\FormRequest;

class DriversFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName' => 'required|alpha',
            'otherName' => 'required|alpha',
            'idNumber' => 'required | alpha_dash | unique:drivers,idNumber',
            'costCenter' => 'required',
            'empNumber' => 'required | alpha_dash | unique:drivers,empNumber',
            'title' => 'required|alpha',
            'licenceNumber' => 'required | alpha_dash | unique:drivers,licenceNumber',
            'licenceClass' => 'required',
            'licenceExpiry' => 'required|date',
        ];
    }
}
