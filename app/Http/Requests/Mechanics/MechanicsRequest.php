<?php

namespace App\Http\Requests\Mechanics;

use Illuminate\Foundation\Http\FormRequest;

class MechanicsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'staffNumber' => 'required|numeric|digits_between:3,6|unique:mechanics,staffNumber',
            'firstName' => 'required|max:10|min:3',
            'lastName' => 'required|max:10|min:3',
            'occupation' => 'required|max:10|min:3',
            'passportPhoto' => 'required',
            'labourRate' => 'required|numeric',
        ];
    }
}
