<?php

namespace App\Http\Requests\Contracted;

use Illuminate\Foundation\Http\FormRequest;

class ContractedUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'   => 'required | exists:contracteds',
            'vehicle'   => 'required',
            'engineCc'  => 'required | numeric | min:1',
            'make'      => 'required | numeric',
            'model'     => 'required | numeric',
            'color'     => 'required',
            'ratePerKm' => 'required | numeric | min:1',
            'ownerName' => 'required'
        ];
    }
}
