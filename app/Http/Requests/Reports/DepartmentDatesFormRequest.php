<?php

namespace App\Http\Requests\Reports;

use App\Http\Requests\FormRequest;
use Carbon\Carbon;
class DepartmentDatesFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'department' => 'required',
            'dateFrom' => 'required | date',
            'dateTo'   => 'required | date | after_or_equal:'.Carbon::parse(request()->dateFrom)->format('jS M Y')
        ];
    }
}
