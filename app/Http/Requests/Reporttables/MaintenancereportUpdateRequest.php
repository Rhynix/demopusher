<?php

namespace App\Http\Requests\Reporttables;

use Illuminate\Foundation\Http\FormRequest;

class MaintenancereportUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3|max:25',
            'userRole' => 'required|numeric',
            'number' => 'required|min:3|max:6',
            'category' => 'required|numeric',
            'description' => 'required|min:3|max:50',
        ];
    }
}
