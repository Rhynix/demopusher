<?php

namespace App\Http\Requests\Models;

use App\Http\Requests\FormRequest;

class ModelsFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'make' => 'required | numeric',
            'description' => 'required | min:3 | max:15 | unique:models,description',
            'serviceInterval' => 'required | numeric',
        ];
    }
}
