<?php

namespace App\Http\Requests\Models;

use App\Http\Requests\FormRequest;

class ModelsUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required | numeric',
            'make' => 'required | numeric',
            'serviceInterval' => 'required | numeric',
            'description' => 'required | min:3 | max:15'
        ];
    }
}
