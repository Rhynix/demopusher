<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName'   => 'required|min:3',
            'lastName'    => 'required|min:3',
            'email'       => 'required|email',
            'costCenter'  => 'required|numeric',
            'role'        => 'required|max:1',
            'avatar'      => 'required'
        ];
    }
}