<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\FormRequest;

class UsersFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName' => 'required|min:3',
            'lastName'  => 'required|min:3',
            'email'     => 'required|unique:users',
            'role'      => 'required|max:1', //between
            'costCenter'=> 'required',
            'avatar'    => 'required',
            'password'  => 'required|alpha_num|min:6|confirmed',
            'phoneNumber' => 'required|digits:12|phone_number',
        ];
    }

    public function messages()
    {
        return [
            'phoneNumber.phone_number' => 'Must be 12-digit phone number (e.g 254711111111) and must not include spaces or special characters',
        ];
    }
}
