<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\FormRequest;

class StoreAvatarFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required|image|max:1024'
        ];
    }

    public function messages()
    {
        return [
            'image.image' => 'Hmmm.., Not a valid image.'
        ];
    }
}
