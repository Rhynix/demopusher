<?php

namespace App\Http\Requests\Itemscategories;

use Illuminate\Foundation\Http\FormRequest;

class ItemscategoriesUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'code' => 'required | numeric | digits:3 | exists:itemscategories,code',
            'description' => 'required|max:30|min:3',
        ];
    }
}
