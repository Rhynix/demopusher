<?php

namespace App\Http\Requests\Taskcodes;

use Illuminate\Foundation\Http\FormRequest;

class TaskcodesUpdateFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'taskCode' => 'required | numeric | digits:3 | exists:taskcodes,taskcode',
            'description' => 'required|max:20|min:3',
        ];
    }
}
