<?php

namespace App\Http\Requests\Itemdetails;

use Illuminate\Foundation\Http\FormRequest;

class ItemdetailsFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dateadded' => 'required|date',
            'quantity' => 'required|numeric',
            'unitCost' => 'required|numeric',
            'totalCost' => 'required|numeric',
            'supplier' => 'required',
        ];
    }
}
