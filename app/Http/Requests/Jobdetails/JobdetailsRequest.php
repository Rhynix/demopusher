<?php

namespace App\Http\Requests\Jobdetails;

use Illuminate\Foundation\Http\FormRequest;

class JobdetailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request()->fromStore == false) {
            return[
                'JobNumber'     => 'required | numeric',
                'vehicle'       => 'required | numeric',
                'quantity'      => 'required | numeric | max:'.request()->availableItems,
                'totalCost'     => 'required | numeric',
                'partDescription'=> 'required | numeric',
                'vendorName'    => 'required | numeric',
                'issueDate'     => 'required | date',
                'unitPrice'     => 'required | numeric',
                'maintenanceType'=> 'required',
            ];
        }else {
            return[
                'partCode'      => 'required|min:3|max:10|alpha_dash',
                'partDesc'      => 'required',
                'JobNumber'     => 'required | numeric',
                'vehicle'       => 'required | numeric',
                'quantity'      => 'required | numeric',
                'totalCost'     => 'required | numeric',
                'vendorName'    => 'required | numeric',
                'issueDate'     => 'required | date',
                'unitPrice'     => 'required | numeric',
                'maintenanceType'=> 'required',
            ];
        }
    }
}
