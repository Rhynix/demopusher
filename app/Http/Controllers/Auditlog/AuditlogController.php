<?php

namespace App\Http\Controllers\Auditlog;

use App\Http\Controllers\Controller;
use App\Models\Auditlog\Auditlog;
use Illuminate\Http\Request;

class AuditlogController extends Controller
{
    public function index()
    {
        $logs = Auditlog::auditlog();
        return response($logs);
    }
}
