<?php

namespace App\Http\Controllers\Messages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Messages\Messages;
use App\Http\Requests\Messages\MessagesFormRequest;

class MessagesController extends Controller
{
    public function store(MessagesFormRequest $request)
    {
        $message = Messages::create([
            'user_id'   => request()->user()->id,
            'to'        => $request->to,
            'message'   => $request->message,
            'status'    => 1
        ]);
        // broadcast( new MessageSent($text))->toOthers();
        return response()->json(['data' => $message ], 201);
    }

    public function show($id)
    {
        $user = request()->user()->id;
        $chat = Messages::messages($id);
        $msg = Messages::where('user_id',$id)->where('to',$user)->where('status',1)->get();

        foreach ($msg as $t) {
            $t->update([
                'status' => 0
            ]);
        }
        return response()->json(['data' => $chat ], 201);
    }
}
