<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Intervention\Image\ImageManager;
use App\Http\Requests\Users\StoreAvatarFormRequest;

class AvatarController extends Controller
{
    protected $imageManager;

    public function __construct(ImageManager $imageManager)
    {
        $this->imageManager = $imageManager;
    }

    public function store(StoreAvatarFormRequest $request)
    {
        $processedImage = $this->imageManager->make($request->file('image')->getPathName())
            ->fit(400, 400, function ($c) {
                $c->aspectRatio();
            })
            ->encode('png')
            ->save(public_path('assets/img/users/') . $path = uniqid(true) . '.png');
        
        return response([
            'data' => [
                'path' => $path
            ]
        ], 200);
    }
}
