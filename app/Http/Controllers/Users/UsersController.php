<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Users\UsersFormRequest;
use App\Models\User\User;
use App\Helper\Newlog;
use App\Http\Requests\Users\UserUpdateRequest;
use File;
use App\Transformers\UsersTransformer;
use App\Notifications\SlackNotification;
use App\Http\Requests\Users\ProfileUpdateRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use App\Events\UserEvent;
use App\Helper\AfricasTalkingGatewayException;
use App\Helper\AfricasTalkingGateway;
use App\Models\Costcenter\Costcenter;

class UsersController extends Controller
{
    public function index(Request $request)
    {
        $users = User::with(['user'])->get();
        return response($users);
    }

    public function getCenterName($center)
    {
        $name = Costcenter::getcentername($center);
        return($name);
    }

    public function userslist(Request $request)
    {
        $users = User::userslist();
        return fractal()
            ->collection($users)
            ->transformWith(new UsersTransformer)
            ->toArray();
    }

    public function store(UsersFormRequest $request)
    {
        if (!in_array(intval($request->role), [1,2,3])) {
            return response()->json(['errors' => ['root' => 'Undefined role']],400);
        }

        $username       = "Rhynix";
        $apikey         = "2a7770007efd0acdc09f4e1cb237cfe2c26e00db77625d4cca36dda36e7d7872";
        $centerName = $this->getCenterName($request->costCenter);
        $gateway = new AfricasTalkingGateway($username, $apikey);

        $message = "Hello $request->firstName $request->lastName, Welcome to Fleet Management system - FLEETWISE. Your Account has been created which belongs to $centerName->reportsname and the password is $request->password. We are glad to have you as a user. Enjoy!!";
        
        $user = User::create([
            'firstname'     => $request->firstName,
            'lastname'      => $request->lastName,
            'email'         => $request->email,
            'password'      => bcrypt($request->password),
            'costCenter'    => $request->costCenter,
            'roleid'        => intval($request->role),
            'accountstatus' => 1,
            'avatar'        => $request->avatar,
            'phone'         => $request->phoneNumber,
            'theme'         => $request->theme,
            'user_id'       => request()->user()->id,
        ]);

        $newuser=User::where('id', $user->id)->with(['user'])->first();
        
        try {
            $results = $gateway->sendMessage("+".$request->phoneNumber, $message);
            foreach($results as $result) {
                $response[] = [
                    'Number' => $result->number,
                    'Status' => $result->status,
                    'StatusCode' => $result->statusCode,
                    'MessageId' => $result->messageId,
                    'Cost' => $result->cost,
                ];
            }
            // \Log::info($response);
            Newlog::addnewlog("User Event", "Created a new user: $request->firstName", 1);
            return response()->json(['data' => $newuser,], 200);
        }
        catch ( AfricasTalkingGatewayException $e){
            return response()->json(['data'=>"Encountered an error while sending SMS ".$e->getMessage()], 500);
        }
    }

    public function updateprofile(ProfileUpdateRequest $request, $id)
    {
        $root = public_path('assets/img/users/');
        File::delete($root.$request->oldAvatar);
        
        $user = User::findOrFail(intval($id));
        broadcast(new UserEvent($user))->toOthers();
        // event(new UserEvent($user));

        if (!empty($request->oldpassword) || !empty($request->password) || !empty($request->password_confirmation)) {
            $request->validate([
                'oldpassword' => 'required|min:6',
                'password'  => 'required|alpha_num|min:6|confirmed',
            ]);
            if (Hash::check($request->oldpassword, $user->password)) {
                $user->update([
                    'firstname' => $request->firstName,
                    'lastname'  => $request->lastName,
                    'phone'     => $request->phoneNumber,
                    'password'  => bcrypt($request->password),
                    'theme'     => $request->theme,
                    'avatar'    => $request->avatar,
                    'user_id'   => request()->user()->id,
                ]);
            }else{
                return new JsonResponse(['errors' => [ 'oldpassword' => ["The old password is incorrect."] ]], 422);
            }
        }else{
            $user->update([
                'firstname' => $request->firstName,
                'lastname'  => $request->lastName,
                'phone'     => $request->phoneNumber,
                'theme'     => $request->theme,
                'avatar'    => $request->avatar,
                'user_id'   => request()->user()->id,
            ]);
        }


        $newuser = User::with(['user'])->findOrFail(intval($user->id));
        Newlog::addnewlog("User Event", "Update user: $newuser->firstname $newuser->lastname", 2);
        $newuser->notify(new SlackNotification($newuser));
        return response()->json(['data'=>$user], 200);
    }

    public function update(UserUpdateRequest $request, $id)
    {
        if (!in_array(intval($request->role), [1,2,3])) {
            return response()->json([
                'data' => 'Undefined role',
            ], 400);
        }
        $root = public_path('assets/img/users/');
        File::delete($root.$request->oldAvatar);

        $user = User::findOrFail(intval($id));
        // $user->update([
        //     'firstname' => $request->firstName,
        //     'lastname'  => $request->lastName,
        //     'email'     => $request->email,
        //     'costCenter'=> $request->costCenter,
        //     'roleid'    => intval($request->role),
        //     'phone'     => $request->phoneNumber,
        //     'theme'     => $request->theme,
        //     'avatar'    => $request->avatar
        // ]);
        $newuser = User::with(['user'])->findOrFail(intval($user->id));
        // Newlog::addnewlog("User Event", "Update user: $request->email", 2);
        broadcast(new UserEvent($newuser))->toOthers();
        $newuser->notify(new SlackNotification($newuser));
        return response()->json(['data'=>$user], 200);
    }

    public function changestate(Request $request, $id)
    {
        $status = $request->status;
        $user = User::findOrFail(intval($id));
        if ($status == 1) {
            $state = 2;
            $budgeaction = "Deactivated";
        }
        else{
            $state = 1;
            $budgeaction = "Activated";
        }
        $user->update([
            'accountstatus' => $state
        ]);
        Newlog::addnewlog("User Event", "$budgeaction $user->firstname's account", 2);
        return response()->json([
            'data' => $user
        ], 200);
    }

    public function changetheme(Request $request, $id)
    {
        $theme = $request->theme;
        $user = User::findOrFail(intval($id));
        if ($theme == 1) {
            $newtheme = 2;
            $budgeaction = "Dark";
        }
        else{
            $newtheme = 1;
            $budgeaction = "Light";
        }
        $user->update([
            'theme' => $newtheme
        ]);
        Newlog::addnewlog("User Event", "changed to $budgeaction theme by $user->firstname", 2);
        return response()->json([
            'data' => $user
        ], 200);
    }
}
