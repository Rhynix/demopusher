<?php

namespace App\Http\Controllers\Models;

use App\Models\Models\Models;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\Newlog;
use App\Http\Requests\Models\ModelsFormRequest;
use App\Http\Requests\Models\ModelsUpdateRequest;
use App\Transformers\ModelsTransformer;

class ModelsController extends Controller
{
    public function index(Request $request)
    {
        $model = Models::models();
        return response($model);
    }

    public function modelslist(Request $request)
    {
        $Models = Models::modelslist();
        return fractal()
            ->collection($Models)
            ->transformWith(new ModelsTransformer)
            ->toArray();
    }

    public function store(ModelsFormRequest $request)
    {
        $model = Models::create([
            'make' => $request->make,
            'description' => $request->description,
            'user_id' => request()->user()->id,
            'costCenter' => request()->user()->costCenter,
            'serviceInter' => $request->serviceInterval,
        ]);

        Newlog::addnewlog("User Event", "Created a new model: $request->description", 1);
        $newmodel = Models::with(["makedesc"])->where("id", $model->id)->first();
        return response()->json(['data' => $newmodel], 201);
    }

    public function update(ModelsUpdateRequest $request, $id)
    {
        $make = Models::findOrFail(intval($id));
        $make->update([
            'make' => $request->make,
            'description' => $request->description,
            'serviceInter' => $request->serviceInterval,
        ]);
        Newlog::addnewlog("User Event", "Updated model: $request->description", 2);
        return response()->json(['data' => $make], 200);
    }

    public function search(Request $request)
    {
        $search = $request->search;
        if ($search != null) {
            $center = request()->user()->costCenter;
            $models = Models::with(["makedesc", "user"])->where('description', 'LIKE', '%'.$search.'%')->orWhereHas('makedesc', function($query) use ($search){
                $query->where('makes.description','LIKE','%'.$search.'%');
            })->orderBy("id", "DESC")->paginate(20);
            return response($models);
        }
    }
}
