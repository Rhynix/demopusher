<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginFormRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\User\User;

class Authcontroller extends Controller
{
    protected $auth;
    protected $maxAttemps = 10;
    protected $decayMinutes = 10;

    public function __construct(JWTAuth $auth)
    {
        $this->auth = $auth;
    }
    
    public function login(LoginFormRequest $request)
    {
        try{
            if (!$token = $this->auth->attempt(['email' => $request->email, 'password' => $request->password, 'accountstatus' => 1])) {
                return response()->json([
                    'errors' => [
                        'root' => 'Incorrect Credentials or Account Suspended'
                    ]
                ], 401);
            }
        } catch (JWTException $e) {
            return response()->json([
                'errors' => [
                    'root' => 'invalid credentials'
                ]
            ], $e->getStatusCode());
        }

        return response()->json([
            'data' => $request->user(),
            'meta' => ['token' => $token]
        ], 200);

    }

    public function logout(Request $request)
    {
        if ($request->id){
            $user = User::findOrFail(intval($request->id));
            $user->update([
                'screenlock' => NULL,
                'online' => NULL,
            ]);
        }

        // Auditlog::create([
        //     'user_id'   => $request->id,
        //     'type'      => "System Event",
        //     'action'    => "Logged Out",
        //     'Code'      => "4",
        //     'costCenter'=> $request->center,
        //     'ip'        => request()->ip()
        // ]);

        $this->auth->invalidate($this->auth->getToken());
        return response(null, 200);
    }

    public function user(Request $request)
    {
        return response()->json([
            'data' => $request->user()
        ]);
    }
}
