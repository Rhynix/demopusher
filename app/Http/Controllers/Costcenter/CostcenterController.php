<?php

namespace App\Http\Controllers\Costcenter;

use App\Models\Costcenter\Costcenter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Costcenter\CostcenterUpdateRequest;
use App\Http\Requests\Costcenter\CotcenterFormRequest;
use App\Transformers\CostcenterTransformer;
use App\Models\Autonumber\Autonumber;
use App\Models\Auditlog\Auditlog;
use App\Helper\Newlog;

class CostcenterController extends Controller
{
    public function index(Request $request)
    {
        $centers = Costcenter::centers();
        return response()->json([
            'data' => $centers,
        ], 200);
    }

    public function selectlist(Request $request)
    {
      $centers = Costcenter::centers();
      return fractal()
           ->collection($centers)
           ->transformWith(new CostcenterTransformer)
           ->toArray();
    }

    public function store(CotcenterFormRequest $request)
    {
        $center = Costcenter::create([
            'code'      => $request->companyCode,
            'company'   => $request->company,
            'reportsname'=> $request->reportsName,
            'address'   => $request->companyAddress,
            'telephone' => $request->companyTelephone,
            'user_id'  => request()->user()->id,
        ]);

        Autonumber::create([
            'costCenter'    => $request->companyCode,
            'jobcardnumber' => "JB0",
            'claiminvoice'  => "CL0",
            'Bookingnumber' => "BK0",
            'workTicket'    => "WT0",
            'stockNumber'   => "ST0",
            'OrderNumber'   => "OD0",
            'tankLevel'     => "TL0",
            'taskNumber'    => "TS0",
            'accidentNumber'=> "AC0",
            'jobNumber'     => "JB0",
            'tripNumber'    => "TN0",
            'violationNumber'=> "VL0",
            'leaveNumber'=> "LN0",
            'user_id'      => request()->user()->id,
        ]);

        Newlog::addnewlog("User Event", "Added new cost center: $request->company with code: $request->companyCode", 1);
        return response()->json(['data' => $center ], 201);
    }

    public function update(CostcenterUpdateRequest $request, $code)
    {
        $center = Costcenter::findOrFail(intval($code));
        $center->update([
            'company'   => $request->company,
            'address'   => $request->companyAddress,
            'reportsname'=> $request->reportsName,
            'telephone' => $request->companyTelephone
        ]);
        Newlog::addnewlog("User Event", "Updated cost center: $request->company", 2);
        return response()->json(['data'=>$center], 200);
    }
}
