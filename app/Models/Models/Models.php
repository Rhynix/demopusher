<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Makes\Makes;
use App\Models\User\User;

class Models extends Model
{
    public $fillable = [
        'make','description','serviceInter','user_id','costCenter'
    ];

    public static function getmodelname($code)
    {
        return static::where('id', $code)->pluck('description')->first();
    }

    public function makedesc()
    {
        return $this->belongsTo(Makes::class, 'make');
    }

    public function user () {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopeModels($query)
    {
        return $query->with(["makedesc","user"])->orderBy("id", "DESC")->paginate(15);
    }

    public function scopeModelslist($query)
    {
        return $query->get();
    }
}
