<?php

namespace App\Models\Messages;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
    public $fillable = [
        'user_id','to','message','status'
    ];

    public function scopeMessages($query, $id)
    {
        $User = request()->user()->id;
        return $query->where(function($q) use ($id) {
            $q->where('user_id', $id)->orWhere('to', $id);
        })->where(function ($q) use ($User) {
            $q->where('user_id', $User)->orWhere('to', $User);
        })->oldest()->get();
    }
}
