<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User\User;

class DueServicesMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    public $user, $service, $pathToFile, $appurl;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $service, $pathToFile, $appurl)
    {
        $this->user = $user;
        $this->service = $service;
        $this->pathToFile = $pathToFile;
        $this->appurl = $appurl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Fleetwise Over Due Services")->view('emails.overdueservices');
    }
}
