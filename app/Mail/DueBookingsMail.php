<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DueBookingsMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    public $user, $bookings, $pathToFile, $appurl;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $bookings, $pathToFile, $appurl)
    {
        $this->user = $user;
        $this->bookings = $bookings;
        $this->pathToFile = $pathToFile;
        $this->appurl = $appurl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Fleetwise booking that need approvals")->view('emails.bookingapprovals');
    }
}
