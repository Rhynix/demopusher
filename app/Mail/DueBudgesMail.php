<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User\User;

class DueBudgesMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    public $user, $password, $pathToFile;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $password, $pathToFile)
    {
        $this->user = $user;
        $this->password = $password;
        $this->pathToFile = $pathToFile;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Fleetwise Over Due Badges")->view('emails.overduebudges');
    }
}
