<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User\User;

class DueLicencesMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    public $user, $Drivers, $pathToFile, $appurl;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $Drivers, $pathToFile, $appurl)
    {
        $this->user = $user;
        $this->Drivers = $Drivers;
        $this->pathToFile = $pathToFile;
        $this->appurl = $appurl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Fleetwise Due Drivers Licences")->view('emails.overduelicences');
    }
}
