<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class AmountOrKilometePrecent implements Rule
{
    public function __construct($amount)
    {
        $this->amount = $amount;
    }
    
    public function passes($attribute, $value)
    {
        return (!empty($value) && !empty($this->amount)) ? false : true;
    }

    public function message()
    {
        return 'Please type either amount or kilometer';
    }
}
