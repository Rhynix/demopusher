<?php
	namespace App\Transformers;

	use App\Models\Models\Models;

	class ModelsTransformer extends \League\Fractal\TransformerAbstract
	{
		public function transform (Models $Makes)
		{
			return[
				'lable' => $Makes->id,
				'value' => $Makes->description
			];
		}
	}
?>