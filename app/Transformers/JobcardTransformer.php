<?php
	namespace App\Transformers;

	use App\Models\Jobcards\Jobcards;

	class JobcardTransformer extends \League\Fractal\TransformerAbstract
	{
		public function transform (Jobcards $Jobcards)
		{
			return[
				'lable' => $Jobcards->JobNumber,
				'value' => $Jobcards->JobNumber,
				'vehicle' => $Jobcards->vehicle,
				'id' => $Jobcards->id,
				'servicetype' => $Jobcards->ServiceType,
				'JobStatus' => $Jobcards->JobStatus
			];
		}
	}
?>