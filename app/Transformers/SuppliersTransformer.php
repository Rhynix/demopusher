<?php
	namespace App\Transformers;

	use App\Models\Suppliers\Suppliers;

	class SuppliersTransformer extends \League\Fractal\TransformerAbstract
	{
		public function transform (Suppliers $Suppliers)
		{
			return[
				'lable' => $Suppliers->id,
				'value' => $Suppliers->name
			];
		}
	}
?>