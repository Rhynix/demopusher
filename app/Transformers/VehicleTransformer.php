<?php
	namespace App\Transformers;

	use App\Models\Vehicles\Vehicles;

	class VehicleTransformer extends \League\Fractal\TransformerAbstract
	{
		public function transform(Vehicles $Vehicles)
		{
			return[
				'lable' => $Vehicles->id,
				'value' => $Vehicles->vehicle,
				'cardnumber' => $Vehicles->cardnumber,
				'chasisnumber' => $Vehicles->chno,
				'fuelType' => $Vehicles->fuelType,
				'fuelname' => $Vehicles->fuelname,
				'department' => $Vehicles->department,
				'odometer' => $Vehicles->odometer,
				'driver' => $Vehicles->driver,
				'model' => $Vehicles->model,
				'vtype' => $Vehicles->vtype,
				'modeldesc' => $Vehicles->modeldesc,
				'deptdesc' => $Vehicles->deptdesc,
				'driverdesc' => $Vehicles->driverdesc,
				'ratePerKm' => $Vehicles->ratePerKm
			];
		}
	}
?>