<?php
	namespace App\Transformers;

	use App\Models\Badges\Badges;

	class BadgesTransformer extends \League\Fractal\TransformerAbstract
	{
		public function transform (Badges $Badges)
		{
			return[
				'lable' => $Badges->id,
				'value' => $Badges->description
			];
		}
	}
?>