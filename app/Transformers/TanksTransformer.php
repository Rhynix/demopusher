<?php
	namespace App\Transformers;

	use App\Models\Tankslevels\Tankslevels;

	class TanksTransformer extends \League\Fractal\TransformerAbstract
	{
		public function transform (Tankslevels $tank)
		{
			return[
				'lable' 	=> $tank->code,
				'value' 	=> $tank->tankname,
				'capacity' 	=> $tank->capacity,
				'fuelType' 	=> $tank->fuelType,
				'fuelname' 	=> $tank->fuelname,
				'lastfilldate' 	=> $tank->daterecieved,
				'lastfilltlrs' 	=> $tank->lastfilllitres,
				'costperliter' 	=> $tank->Lastcostperliter,
				'originalbalance' 	=> $tank->Originalbalance,
				'remainingbalance' 	=> $tank->remainbalance,
				'lastsupplire' 	=> $tank->lastsupplire,
			];
		}
	}
?>