<?php
	namespace App\Transformers;

	use App\Models\Contracteds\Contracteds;

	class ContractedlistTransformer extends \League\Fractal\TransformerAbstract
	{
		public function transform (Contracteds $Contracteds)
		{
			return[
				'lable' => $Contracteds->vehicle,
				'value' => $Contracteds->vehicle,
				'ratePerKm' => $Contracteds->ratePerKm
			];
		}
	}
?>