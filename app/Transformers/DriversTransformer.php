<?php
	namespace App\Transformers;

	use App\Models\Drivers\Drivers;

	class DriversTransformer extends \League\Fractal\TransformerAbstract
	{
		public function transform (Drivers $Drivers)
		{
			return[
				'lable' => $Drivers->id,
				'empNumber' => $Drivers->empNumber,
				'value' => $Drivers->firstName.' '.$Drivers->otherName,
				'mobile'=> $Drivers->mobile
			];
		}
	}
?>