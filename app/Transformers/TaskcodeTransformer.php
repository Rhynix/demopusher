<?php
	namespace App\Transformers;

	use App\Models\Taskcodes\Taskcodes;

	class TaskcodeTransformer extends \League\Fractal\TransformerAbstract
	{
		public function transform (Taskcodes $Taskcodes)
		{
			return[
				'lable' => $Taskcodes->taskcode,
				'value' => $Taskcodes->description
			];
		}
	}
?>