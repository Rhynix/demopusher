<?php
	namespace App\Transformers;

	use App\Models\Fueltypes\Fueltypes;

	class FuelTransformer extends \League\Fractal\TransformerAbstract
	{
		public function transform (Fueltypes $Fueltypes)
		{
			return[
				'lable' => $Fueltypes->code,
				'value' => $Fueltypes->name,
				'unitcost' => $Fueltypes->unitCost,
			];
		}
	}
?>