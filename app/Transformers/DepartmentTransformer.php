<?php
	namespace App\Transformers;

	use App\Models\Departments\Departments;

	class DepartmentTransformer extends \League\Fractal\TransformerAbstract
	{
		public function transform (Departments $Departments)
		{
			return[
				'lable' => $Departments->department,
				'value' => $Departments->description,
				'account' => $Departments->account
				
			];
		}
	}
?>