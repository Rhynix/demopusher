<?php
	namespace App\Transformers;

	use App\Models\Vehicletype\Vehicletype;

	class VehicletypeTransformer extends \League\Fractal\TransformerAbstract
	{
		public function transform (Vehicletype $Vehicletype)
		{
			return[
				'lable' => $Vehicletype->id,
				'value' => $Vehicletype->description
			];
		}
	}
?>