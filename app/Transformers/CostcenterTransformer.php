<?php
	namespace App\Transformers;

	use App\Models\Costcenter\Costcenter;

	class CostcenterTransformer extends \League\Fractal\TransformerAbstract
	{
		public function transform (Costcenter $center)
		{
			return[
				'lable' => $center->code,
				'value' => $center->company,
				'telephone' => $center->telephone
			];
		}
	}
?>