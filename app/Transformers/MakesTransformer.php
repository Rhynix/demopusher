<?php
	namespace App\Transformers;

	use App\Models\Makes\Makes;

	class MakesTransformer extends \League\Fractal\TransformerAbstract
	{
		public function transform (Makes $Makes)
		{
			return[
				'lable' => $Makes->id,
				'value' => $Makes->description
			];
		}
	}
?>