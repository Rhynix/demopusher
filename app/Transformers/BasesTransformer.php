<?php
	namespace App\Transformers;

	use App\Models\Bases\Bases;

	class BasesTransformer extends \League\Fractal\TransformerAbstract
	{
		public function transform (Bases $Bases)
		{
			return[
				'lable' => $Bases->id,
				'value' => $Bases->description
			];
		}
	}
?>