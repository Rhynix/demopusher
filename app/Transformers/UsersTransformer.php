<?php
	namespace App\Transformers;

	use App\Models\User\User;

	class UsersTransformer extends \League\Fractal\TransformerAbstract
	{
		public function transform (User $User)
		{
			return[
				'lable' => $User->id,
				'value' => $User->firstname.' '.$User->lastname,
				'names' => $User->firstname,
				'center' => $User->costCenter,
				'roleid' => $User->roleid,
				'avatar' => $User->avatar,
				'phone' => $User->phone,
				'email' => $User->email,
				'accountstatus' => $User->accountstatus,
			];
		}
	}
?>