<?php
	namespace App\Transformers;

	use App\Models\Vendors\Vendors;

	class VendorTransformer extends \League\Fractal\TransformerAbstract
	{
		public function transform (Vendors $Vendors)
		{
			return[
				'lable' => $Vendors->id,
				'value' => $Vendors->description
			];
		}
	}
?>