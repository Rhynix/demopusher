<?php
	namespace App\Transformers;

	use App\Models\Servicetypes\Servicetypes;

	class ServicetypeTransformer extends \League\Fractal\TransformerAbstract
	{
		public function transform (Servicetypes $Servicetypes)
		{
			return[
				'lable' => $Servicetypes->id,
				'value' => $Servicetypes->description
			];
		}
	}
?>