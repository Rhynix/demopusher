<?php
	namespace App\Transformers;

	use App\Models\Bodytypes\Bodytypes;

	class BodytypesTransformer extends \League\Fractal\TransformerAbstract
	{
		public function transform (Bodytypes $Bodytypes)
		{
			return[
				'lable' => $Bodytypes->id,
				'value' => $Bodytypes->description
			];
		}
	}
?>