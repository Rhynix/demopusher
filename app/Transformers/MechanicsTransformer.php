<?php
	namespace App\Transformers;

	use App\Models\Mechanics\Mechanics;

	class MechanicsTransformer extends \League\Fractal\TransformerAbstract
	{
		public function transform (Mechanics $Mechanics)
		{
			return[
				'lable' => $Mechanics->staffNumber,
				'value' => $Mechanics->firstName.' '.$Mechanics->lastName,
				'labourRate' => $Mechanics->labourRate,
			];
		}
	}
?>