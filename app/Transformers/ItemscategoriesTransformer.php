<?php
	namespace App\Transformers;
	use App\Models\Itemscategories\Itemscategories;

	class ItemscategoriesTransformer extends \League\Fractal\TransformerAbstract
	{
		public function transform (Itemscategories $Itemscategories)
		{
			return[
				'lable' => $Itemscategories->id,
				'value' => $Itemscategories->description
			];
		}
	}
?>