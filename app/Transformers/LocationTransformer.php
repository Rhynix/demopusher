<?php
	namespace App\Transformers;

	use App\Models\Locations\Locations;


	class LocationTransformer extends \League\Fractal\TransformerAbstract
	{
		public function transform(Locations $Locations)
		{
			return[
				'lable' => $Locations->location,
				'value' => $Locations->description,
				'isTank' => $Locations->isTank,
				'costCenter' => $Locations->costCenter
			];
		}
	}
?>