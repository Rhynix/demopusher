<?php
	namespace App\Transformers;
	use App\Models\Items\Items;
	class ItemsTransformer extends \League\Fractal\TransformerAbstract
	{
		public function transform (Items $Items)
		{
			return[
				'lable' 	=> $Items->id,
				'value' 	=> $Items->partDesc.' - '.$Items->partCode,
				'unitDesc' 	=> $Items->unitDesc,
				'Lastprice' => $Items->Lastprice,
				'stocknumb' => $Items->stocknumb,
				'available' => $Items->available
			];
		}
	}
?>