<?php
	namespace App\Transformers;

	use App\Models\Maintenancetypes\Maintenancetypes;

	class MaintenanceTransformer extends \League\Fractal\TransformerAbstract
	{
		public function transform (Maintenancetypes $Maintenancetypes)
		{
			return[
				'lable' => $Maintenancetypes->id,
				'value' => $Maintenancetypes->description
			];
		}
	}
?>