<?php

namespace App\Events\Reports;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class PoolreportsEvent
{
    use Dispatchable, SerializesModels;

    public $slug;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($slug)
    {
        $this->slug = $slug;
    }
}
