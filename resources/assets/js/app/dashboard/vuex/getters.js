export const getVehicleJobcardCounter = (state) => {
    return state.vehicleJobcardCounter
}

export const getServicescount = (state) => {
    return state.servicescount
}

export const getLeave = (state) => {
    return state.leavecount
}

export const getFuelmonth = (state) => {
    return state.fuelmonth
}

export const getBookingmonth = (state) => {
    return state.bookingmonth
}

export const getMechanicsJob = (state) => {
    return state.mechanicsjob
}

export const getBookingChart = (state) => {
    return state.bookingChart
}

export const getOpenJobcards = (state) => {
    return state.jobcards
}

export const getFuelChat = (state) => {
    return state.fuelchart
}