import dashboard from './dashboard/routes'
import users from './users/routes'
import messages from './messages/routes'
import auth from './auth/routes'
import errors from './errors/routes'
import welcome from './welcome/routes'
import costcenters from './costcenters/routes'

export default 
[
    ...dashboard,
    ...users,
    ...messages,
    ...auth,
    ...welcome,
    ...costcenters,
    ...errors
]