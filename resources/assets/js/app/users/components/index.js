import Vue from 'vue'
export const Users = Vue.component('users', require('./Users'))
export const Usersform = Vue.component('usersform', require('./Usersform'))
export const Userdata = Vue.component('userdata', require('./Userdata'))
export const Imagerupload = Vue.component('imagerupload', require('./Imagerupload'))
export const Profile = Vue.component('profile', require('./Profile'))