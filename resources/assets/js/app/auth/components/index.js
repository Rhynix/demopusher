import Vue from 'vue'
export const Lockscreen = Vue.component('lockscreen', require('./Lockscreen.vue'))
export const Login = Vue.component('login', require('./Login.vue'))
export const Resetpassword = Vue.component('resetpassword', require('./Resetpassword.vue'))