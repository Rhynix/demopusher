import Vue from 'vue'
import Vuex from 'vuex'
import dashboard from '../app/dashboard/vuex'
import auth from '../app/auth/vuex'
import users from '../app/users/vuex'
import costcenters from '../app/costcenters/vuex'
import messages from '../app/messages/vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        dashboard: dashboard,
        auth: auth,
        users: users,
        costcenters: costcenters,
        messages: messages,
    }
})