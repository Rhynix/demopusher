
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import router from './router'
import VModal from 'vue-js-modal'
import ElementUI from 'element-ui'
import store from './vuex'
import locale from 'element-ui/lib/locale/lang/en'
import localforage from 'localforage'
Vue.use(VModal, {dialog: true})
Vue.use(ElementUI, { locale })

localforage.config({
    driver: localforage.LOCALSTORAGE,
    storeName: 'fleetwise'
});
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('app', require('./components/App.vue'));
Vue.component('leftmenu', require('./components/Leftmenu.vue'));
Vue.component('allcontent', require('./components/Content.vue'));
Vue.component('mobilemenu', require('./components/Mobilemenu.vue'));
Vue.component('topbarmenu', require('./components/Topbarmenu.vue'));
Vue.component('chat', require('./components/Chat.vue'));
Vue.component('reportModal', require('./components/reportModal.vue'));
Vue.component('reportCard', require('./components/reportCard.vue'));
Vue.component('swalMessageModal', require('./components/swalMessageModal.vue'));
Vue.component('qm', require('./components/swalQuestionModal.vue'));
Vue.component('formerror', require('./components/Formerror.vue'));
Vue.component('fuelcard', require('./components/fuelCard.vue'));
Vue.component('report-settings-modal', require('./components/reportSettingsModal.vue'));
Vue.component('data-pages', require('./components/dataPages.vue'));
Vue.component('loading', require('./components/loadinganimation.vue'));
Vue.component('loading', require('./components/loadinganimation.vue'));
Vue.component('reptanim', require('./components/reportAnimation.vue'));
Vue.component('bkModal', require('./components/bookingModal.vue'));
Vue.component('todolist', require('./components/todolist.vue'));
Vue.component('asteriks', require('./components/asteriks.vue'));
Vue.component('vehicleReportModal', require('./components/vehicleReportModal.vue'));
Vue.component('poolReportModal', require('./components/poolReportModal.vue'));
Vue.component('costingReportModal', require('./components/costingReportModal.vue'));
Vue.component('fuelReportModal', require('./components/fuelReportModal.vue'));
Vue.component('repairReportModal', require('./components/repairReportModal.vue'));
Vue.component('inventoryReportModal', require('./components/inventoryReportModal.vue'));
Vue.component('driversReportModal', require('./components/driversReportModal.vue'));
Vue.component('accidentReportModal', require('./components/accidentReportModal.vue'));
Vue.component('tripsReportModal', require('./components/tripsReportModal.vue'));
Vue.component('reportGroupingCard', require('./components/reportGroupingCard.vue'));
Vue.component('reportListing', require('./components/reportListing.vue'));

store.dispatch('auth/setToken').then(() => {
    store.dispatch('auth/fetchUser').catch(() => {
        store.dispatch('auth/clearAuth')
        router.replace({ name: 'login' })
    })
}).catch(() => {
    store.dispatch('auth/clearAuth')
});

const app = new Vue({
    el: '#app',
    router: router,
    store: store
});
