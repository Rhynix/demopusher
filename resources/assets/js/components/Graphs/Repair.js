import { Bar } from 'vue-chartjs'
export default {
    props: ['chart'],
    extends: Bar,
    methods: {
        recievedata() {
            var vm = this.chart;
            var cr = vm.curr;
            var pr = vm.prev;
            var p = vm.p;
            var y = vm.y;
            this.gradient = this.$refs.canvas.getContext('2d').createLinearGradient(0, 0, 0, 450)
            this.gradient2 = this.$refs.canvas.getContext('2d').createLinearGradient(0, 0, 0, 450)

            this.gradient.addColorStop(0, 'rgb(255, 31, 45)');
            this.gradient.addColorStop(0.5, 'rgb(255, 31, 45)');
            this.gradient.addColorStop(1, 'rgb(255, 31, 45)');
            
            this.gradient2.addColorStop(0, 'rgb(47, 103, 229)')
            this.gradient2.addColorStop(0.5, 'rgb(47, 103, 229)');
            this.gradient2.addColorStop(1, 'rgb(47, 103, 229)');

            this.renderChart({
                labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                datasets: [
                    {
                        label: y,
                        borderColor: '#05CBE1',
                        pointBackgroundColor: '#05CBE1',
                        borderWidth: 0,
                        hoverBackgroundColor: '#BED6F6',
                        barThickness: 0.3,
                        maxBarThickness: 1,
                        pointBorderColor: 'white',
                        backgroundColor: this.gradient2, 
                        data: [cr.repairJan, cr.repairFeb, cr.repairMar, cr.repairApr, cr.repairMay, cr.repairJun, cr.repairJul, cr.repairAug, cr.repairSep, cr.repairOct, cr.repairNov, cr.repairDec]
                    },
                    {
                        label: p,
                        borderColor: '#FC2525',
                        pointBackgroundColor: '#FC2525',
                        borderWidth: 0,
                        hoverBackgroundColor: '#BED6F6',
                        barThickness: 0.3,
                        maxBarThickness: 1,
                        pointBorderColor: 'white',
                        backgroundColor: this.gradient,
                        data: [pr.repairJan, pr.repairFeb, pr.repairMar, pr.repairApr, pr.repairMay, pr.repairJun, pr.repairJul, pr.repairAug, pr.repairSep, pr.repairOct, pr.repairNov, pr.repairDec]
                    }
                ],
                options: {
                    // This chart will not respond to mousemove, etc
                    events: ['click']
                }
            },
            {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    xAxes: [{
                        barPercentage: 0.5,
                        ticks: {
                            // fontSize: 40,
                            fontColor: '#000000',
                        },
                        gridLines: {
                            color: "rgba(0, 0, 0, 0)",
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            // fontSize: 40,
                            fontColor: '#000000',
                        },
                        gridLines: {
                            color: "rgba(0, 0, 0, 0)",
                        }
                    }]
                },
                legend: {
                    labels: {
                        fontColor: '#000000',
                        boxWidth: 14
                    }
                },
                title: {
                    title: {
                        fontColor: '#000000'
                    }
                }
            })
        },
    },
    watch: {
        chart: function() {
            this.recievedata()
        }
    },
    mounted () {
        this.recievedata()
    }
}