<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Fleetwise</title>
        @include('includes.css')
    </head>
    {{-- <body class="full-screen color-scheme-dark menu-position-side menu-side-left"> --}}
    {{-- <body class="menu-position-side menu-side-left full-screen">
        <div class="all-wrapper with-side-panel solid-bg-all"> --}}
            <span id="app">
                <app></app>
            </span>
        {{-- </div>
    </body> --}}
    @include('includes.js')
    <script>
        $('input#search').quicksearch('.has-sub-menu a');
        </script>
</html>
