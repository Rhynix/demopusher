<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">

<style type="text/css">

div, p, a, li, td { -webkit-text-size-adjust:none; }

.ReadMsgBody
{width: 100%; background-color: #ffffff;}
.ExternalClass
{width: 100%; background-color: #ffffff;}
body{width: 100%; height: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;}
html{width: 100%;}

@font-face {
    font-family: 'proxima_novalight';src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-light-webfont.eot');src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-light-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-light-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-light-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}

@font-face {
    font-family: 'proxima_nova_rgregular'; src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-regular-webfont.eot');src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-regular-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-regular-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-regular-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}

@font-face {
    font-family: 'proxima_novasemibold';src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-semibold-webfont.eot');src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-semibold-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-semibold-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-semibold-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
    
@font-face {
	font-family: 'proxima_nova_rgbold';src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-bold-webfont.eot');src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-bold-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-bold-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-bold-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
	
@font-face {
    font-family: 'proxima_novablack';src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-black-webfont.eot');src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-black-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-black-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-black-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
    
@font-face {font-family: 'proxima_novathin';src: url('http://rocketway.net/themebuilder/template/templates/mason/font/proximanova-thin-webfont.eot');src: url('http://rocketway.net/themebuilder/template/templates/mason/font/proximanova-thin-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/template/templates/mason/font/proximanova-thin-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/template/templates/mason/font/proximanova-thin-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}

p {padding: 0!important; margin-top: 0!important; margin-right: 0!important; margin-bottom: 0!important; margin-left: 0!important; }

.hover:hover {opacity:0.85;filter:alpha(opacity=85);}

.image77 img {width: 77px; height: auto;}
.avatar125 img {width: 125px; height: auto;}
.image75 img {width: 156px; height: auto;}
.icon18 img {width: 18px; height: auto;}
.icon61 img {width: 35px;
height: 35px;
overflow: hidden;
border-radius: 50%;
margin: 0 auto;
box-shadow: 0px 0px 1px 1px rgba(0, 0, 0, 0.1), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2);}

</style>

<!-- @media only screen and (max-width: 640px) 
		   {*/
		   -->
<style type="text/css"> @media only screen and (max-width: 640px){
		body{width:auto!important;}
		table[class=full2] {width: 100%!important; clear: both; }
		table[class=mobile2] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
		table[class=fullCenter2] {width: 100%!important; text-align: center!important; clear: both; }
		td[class=fullCenter2] {width: 100%!important; text-align: center!important; clear: both; }
		td[class=pad15] {width: 100%!important; padding-left: 15px; padding-right: 15px; clear: both;}
		
} </style>
<!--

@media only screen and (max-width: 479px) 
		   {
		   -->
<style type="text/css"> @media only screen and (max-width: 479px){
		body{width:auto!important;}
		table[class=full2] {width: 100%!important; clear: both; }
		table[class=mobile2] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
		table[class=fullCenter2] {width: 100%!important; text-align: center!important; clear: both; }
		td[class=fullCenter2] {width: 100%!important; text-align: center!important; clear: both; }
		table[class=full] {width: 100%!important; clear: both; }
		table[class=mobile] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
		table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
		td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
		td[class=pad15] {width: 100%!important; padding-left: 15px; padding-right: 15px; clear: both;}
		.erase {display: none;}
				
		}
} </style>



<!-- Notification 6 -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full2"  bgcolor="#303030"style="background-color: rgb(48, 48, 48);">
	<tr>
		<td style="background-image: url({{$message->embed(public_path().'/images/not5_bg_image.jpg')}}); -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; background-position: center center; background-repeat: no-repeat; background-attachment: fixed;" id="not6">
		
			
			<!-- Mobile Wrapper -->
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2">
				<tr>
					<td width="100%">
					
						<div class="sortable_inner ui-sortable">
						<!-- Space -->
						<table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module-small">
							<tr>
								<td width="100%" height="50"></td>
							</tr>
						</table><!-- End Space -->
			
						<!-- Start Top -->
						<table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#4edeb5" style="border-top-left-radius: 5px; border-top-right-radius: 5px; background-color: rgb(255, 255, 255);" object="drag-module-small">
							<tr>
								<td width="100%" valign="middle" class="image75">
									
									<!-- Header Text --> 
									<table width="540" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter2">
										<tr>
											<td width="100%" height="30"></td>
										</tr>
										<tr>
											<td width="100%"><span ><img src="{{$message->embed(public_path().$pathToFile)}}" width="156" alt="" border="0" ></span></td>
										</tr>
										<tr>
											<td width="100%" height="30"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						
						<table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#ffffff"object="drag-module-small" style="background-color: rgb(255, 255, 255);">
							<tr>
								<td width="100%" valign="middle">
								 
									<table width="640" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter2">
										<tr>
											<td width="100%" height="30"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						
						<table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#ffffff"object="drag-module-small" style="background-color: rgb(255, 255, 255);">
							<tr>
								<td width="100%" valign="middle">
								 
									<table width="640" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter2">
										<tr>
											<td valign="middle" width="100%" style="text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 23px; color: rgb(63, 67, 69); line-height: 30px; font-weight: 100;">
												<!--[if !mso]><!--><span style="font-family: 'proxima_novathin', Helvetica; font-weight: normal;"><!--<![endif]-->Hi {{ ucwords($user->firstname.' '.$user->lastname) }}. <!--[if !mso]><!--></span><!--<![endif]-->
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>

						<table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#ffffff"object="drag-module-small" style="background-color: rgb(255, 255, 255);">
							<tr>
								<td width="100%" valign="middle">
								 
									<table width="640" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter2">
										<tr>
											<td valign="middle" width="100%" style="text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: rgb(63, 67, 69); line-height: 24px;">
												<!--[if !mso]><!--><span style="font-family: 'proxima_nova_rgregular', Helvetica; font-weight: normal;"><!--<![endif]-->
													Below is a list of Bookings that urgently needs your approval / rejection,<br/>
													{{ucwords(strtolower($user->centerdesc['reportsname']))}},<br/>
													{{ucwords(strtolower($user->centerdesc['company']))}} branch,<br/>
													Please attend to them.
												<!--[if !mso]><!--></span><!--<![endif]-->
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						
						<table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#ffffff"object="drag-module-small" style="background-color: rgb(255, 255, 255);">
							<tr>
								<td width="100%" valign="middle">
								 
									<table width="640" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter2">
										<tr>
											<td width="100%" height="30"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>

						<table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#E4FAFE"object="drag-module-small" style="background-color: rgb(255, 255, 255);">
								<tr>
									<td width="100%" height="10"></td>
								</tr>
								<tr>
									<td width="100%" valign="middle" class="icon61">
										<table width="640" border="0" cellpadding="0" cellspacing="0" align="center" style=" border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
											<tr>
												<td>
													<table border="0" cellpadding="0" cellspacing="0" align="left"> 
														<tr> 
															<td align="center" width="595" height="25"bgcolor="#4edeb5" style="border-top-left-radius: 5px; border-top-right-radius: 5px; border-bottom-right-radius: 5px; border-bottom-left-radius: 5px; padding-left: 10px; text-align: left; padding-right: 10px; font-weight: bold; font-family: Helvetica; color: rgb(0, 0, 0); background-color: rgb(255, 255, 255);">
																<span style="width: 100%;">
																	<table style="font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: rgb(0, 0, 0); font-size: 12px; font-weight: 700; text-decoration: none; line-height: 10px;">
																		<tr>
																			<td width="103" valign="left">
																				BK. No
																			</td>
																			<td width="102" valign="left">
																				Vehicle.
																			</td>
																			<td width="145" valign="left">
																				Date Booked
																			</td>
																			<td width="120" valign="left">
																				Destination
																			</td>
																			<td width="175" valign="left">
																				Reason
																			</td>
																		</tr>
																	</table>
																</span>
															</td> 
														</tr> 
													</table> 
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>


						@foreach($bookings as $bg)
						@if($user->costcenter == $bg['costCenter'])
						<table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#E4FAFE"object="drag-module-small" style="background-color: rgb(255, 255, 255);">
							<tr>
								<td width="100%" height="10"></td>
							</tr>
							<tr>
								<td width="100%" valign="middle" class="icon61">
									<table width="640" border="0" cellpadding="0" cellspacing="0" align="center" style=" border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
										<tr>
											<td>
												<table border="0" cellpadding="0" cellspacing="0" align="left"> 
													<tr> 
														<td align="center" width="595" height="25"bgcolor="#4edeb5" style="border-top-left-radius: 5px; border-top-right-radius: 5px; border-bottom-right-radius: 5px; border-bottom-left-radius: 5px; padding-left: 10px; text-align: left; padding-right: 10px; font-weight: bold; font-family: Helvetica; color: rgb(255, 255, 255); background-color: rgb(222, 94, 35);">
															<span style="width: 100%;">
																<table style="font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: rgb(255, 255, 255); font-size: 12px; text-decoration: none; line-height: 10px;">
																	<tr>
																		<td width="103" valign="left">
																			{{$bg['rentalNumber']}}
																		</td>
																		<td width="102" valign="left">
																			{{$bg['vehicle']}}
																		</td>
																		<td width="145" valign="left">
																			{{$bg['datentime']}}
																		</td>
																		<td width="120" valign="left">
																			{{$bg['destination']}}
																		</td>
																		<td width="175" valign="left">
																			{{$bg['reasons']}}
																		</td>
																	</tr>
																</table>
															</span>
														</td> 
													</tr> 
												</table> 
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						@endif
						@endforeach

						
						<table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#ffffff"object="drag-module-small" style="background-color: rgb(255, 255, 255);">
							<tr>
								<td width="100%" valign="middle">
								 
									<table width="640" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter2">
										<tr>
											<td width="100%" height="40"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						
						<table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#ffffff"object="drag-module-small" style="background-color: rgb(255, 255, 255);">
							<tr>
								<td width="100%" valign="middle">
								 
									<table width="640" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter2">
										<tr>
											<td>
												<table border="0" cellpadding="0" cellspacing="0" align="left"> 
													<tr> 
														<td align="center" height="30"bgcolor="#E40360" style="border-top-left-radius: 5px; border-top-right-radius: 5px; border-bottom-right-radius: 5px; border-bottom-left-radius: 5px; padding-left: 20px; padding-right: 20px; font-weight: bold; font-family: Helvetica, Arial, sans-serif; color: rgb(255, 255, 255); background-color: rgb(211, 114, 4);">
															<!--[if !mso]><!--><span style="font-family: 'proxima_nova_rgbold', Helvetica; font-weight: normal;"><!--<![endif]-->
																<a href="{{$appurl}}" style="color: rgb(255, 255, 255); font-size: 14px; text-decoration: none; line-height: 10px; width: 100%;">Login</a>
															<!--[if !mso]><!--></span><!--<![endif]-->
														</td> 
													</tr> 
												</table> 
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>

						<table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#ffffff"object="drag-module-small" style="background-color: rgb(255, 255, 255);">
							<tr>
								<td width="100%" valign="middle">
								 
									<table width="640" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter2">
										<tr>
											<td width="100%" height="35"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						
						<table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#ffffff"object="drag-module-small" style="background-color: rgb(255, 255, 255);">
							<tr>
								<td width="100%" valign="middle">
								 
									<table width="640" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter2">
										<tr>
											<td valign="middle" width="100%" style="text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: rgb(63, 67, 69); line-height: 24px;">
												<!--[if !mso]><!--><span style="font-family: 'proxima_nova_rgregular', Helvetica; font-weight: normal;"><!--<![endif]-->
												Regards!
												<br>
												Fleetwise Management System Team.
												<!--[if !mso]><!--></span><!--<![endif]-->
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						
						<table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#ffffff"style="border-bottom-left-radius: 5px; border-bottom-right-radius: 5px; background-color: rgb(255, 255, 255);" object="drag-module-small">
							<tr>
								<td width="100%" valign="middle">
								 
									<table width="640" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter2">
										<tr>
											<td width="100%" height="50"></td>
										</tr>
									</table>
																	
								</td>
							</tr>
						</table>
						
						<table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="full2" object="drag-module-small">
							<tr>
								<td width="100%" height="30"></td>
							</tr>
						</table>
						
						<table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" object="drag-module-small">
							<tr>
								<td valign="middle" width="100%" style="text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 13px; color: rgb(255, 255, 255); line-height: 24px; font-style: italic;">
									<!--[if !mso]><!--><span style="font-family: 'proxima_nova_rgregular', Helvetica; font-weight: normal;"><!--<![endif]-->For more help, contact the system admin. <!--<![endif]--></span><!--[if !mso]><!-->
								</td>
							</tr>
						</table>
						
						
						<table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" object="drag-module-small">
							<tr>
								<td width="100%" height="30"></td>
							</tr>
						</table>
						
						<table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" object="drag-module-small">
							<tr>
								<td width="100%" height="29"></td>
							</tr>
							<tr>
								<td width="100%" height="1"></td>
							</tr>
						</table>
						</div>
						
					</td>
				</tr>
			</table>
			
		</div>
		</td>
	</tr>
</table><!-- End Notification 6 -->
</div>	<style>body{ background: none !important; } </style>