<?php

use Illuminate\Http\Request;

Route::post('/auth/login', 'Auth\Authcontroller@login');
Route::post('/auth/logout', 'Auth\Authcontroller@logout');
Route::post('/auth/logout', 'Auth\Authcontroller@logout');
Route::post('/auth/passwordreset', 'Auth\ForgotCredentialsController@resetPassword');

Broadcast::routes(['middleware' => ['jwt.auth']]);
Route::group(['middleware' => 'jwt.auth'], function () {
    Route::get('/auth/user', 'Auth\Authcontroller@user');
    // User
    Route::post('/user/store', 'Users\UsersController@store');
    Route::post('/user/avatar', 'Users\AvatarController@store');
    Route::get('/users', 'Users\UsersController@index');
    Route::put('/changestate/{id}/status', 'Users\UsersController@changestate');
    Route::put('/users/{id}/update','Users\UsersController@update');
    Route::put('/profile/{id}/update','Users\UsersController@updateprofile');
    Route::get('/userslist', 'Users\UsersController@userslist');

    // Messages
    Route::post('/message/create', 'Messages\MessagesController@store');
    Route::get('/showmessage/{id}', 'Messages\MessagesController@show');
    
    // Cost centers
    Route::get('/costcenters', 'Costcenter\CostcenterController@index');
    Route::post('/center/create', 'Costcenter\CostcenterController@store');
    Route::put('/costcenter/{id}/update', 'Costcenter\CostcenterController@update');
    Route::get('/centerlist', 'Costcenter\CostcenterController@selectlist');

    
    
});
